import pytest
import json

from tests.test_todos import setup_todos


headers = {
    "Content-Type": "application/json"
}


def setup_task(client, task=None):
    setup_todos(client, todos=[{"name": "Shiny TODO"}])
    test_task = task or {
        "name": "Do this",
        "todo_id": 1,
        "description": "Do this properly",
        "complete": False
    }
    response = client.post('/tasks', data=json.dumps(test_task), headers=headers)
    assert '201' in response.status


def test_no_tasks(client):
    response = client.get('/tasks', headers=headers)
    assert '200' in response.status
    assert [] == response.get_json().get('data')


@pytest.mark.parametrize("test_task_data", [
    {
         "name": "Do this",
         "todo_id": 1,
         "description": "Do this properly",
         "complete": False
    },
    {
        "name": "Do this",
        "todo_id": 1,
        "description": "Do this properly",
        "complete": True
    },
    {
        "name": "Do this",
        "todo_id": 1,
        "description": "Do this properly",
    }
])
def test_post_task_success(client, test_task_data):
    setup_todos(client, todos=[{"name": "Shiny TODO"}])
    response = client.post('/tasks', data=json.dumps(test_task_data), headers=headers)
    assert '201' in response.status
    assert all(item in response.get_json().get('data') for item in test_task_data)


@pytest.mark.parametrize("test_task_data", [
    {
         "todo_id": 1,
         "description": "Do this properly",
    },
    {
        "name": "Do this",
        "description": "Do this properly",
    },
    {
        "name": "Do this",
        "todo_id": 1,
    },
    {},
])
def test_post_task_failure(client, test_task_data):
    setup_todos(client, todos=[{"name": "Shiny TODO"}])
    response = client.post('/tasks', data=json.dumps(test_task_data), headers=headers)
    assert '422' in response.status


def test_post_task_todo_not_found_failure(client):
    test_task_data = {
        "name": "Do this",
        "todo_id": 1,
        "description": "Do this properly",
    }
    response = client.post('/tasks', data=json.dumps(test_task_data), headers=headers)
    assert '404' in response.status


def test_get_tasks_success(client):
    setup_task(client)
    expected_tasks = [
        {
            "id": 1,
            "name": "Do this",
            "todo_id": 1,
            "description": "Do this properly",
            "complete": False
        }
    ]
    response = client.get('/tasks', headers=headers)
    assert '200' in response.status
    assert expected_tasks == response.get_json().get('data')


def test_get_tasks_by_id_success(client):
    setup_task(client)
    expected_tasks = {
        "id": 1,
        "name": "Do this",
        "todo_id": 1,
        "description": "Do this properly",
        "complete": False
    }
    response = client.get('/tasks/1', headers=headers)
    assert '200' in response.status
    assert expected_tasks == response.get_json().get('data')


def test_get_tasks_filtered_by_todo_id_success(client):
    setup_task(client)
    setup_task(client, task={
        "name": "Also do this",
        "todo_id": 1,
        "description": "Don't Do this properly",
        "complete": False
    })
    expected_tasks = [{
        "id": 1,
        "name": "Do this",
        "todo_id": 1,
        "description": "Do this properly",
        "complete": False
    },
    {
        "id": 2,
        "name": "Also do this",
        "todo_id": 1,
        "description": "Don't Do this properly",
        "complete": False
    }]
    response = client.get('/tasks?todo_id=1', headers=headers)
    assert '200' in response.status
    assert expected_tasks == response.get_json().get('data')


def test_delete_task(client):
    setup_task(client)
    response = client.delete('/tasks/1', headers=headers)
    assert '200' in response.status

    response = client.get('/tasks/1', headers=headers)
    assert {} == response.get_json().get('data')


def test_edit_task(client):
    setup_task(client)
    update_data = {
        "description": "Don't Do this properly",
    }
    expected_data = {
        "id": 1,
        "name": "Do this",
        "todo_id": 1,
        "description": "Don't Do this properly",
        "complete": False
    }
    response = client.put('/tasks/1', headers=headers, data=json.dumps(update_data))
    assert "200" in response.status
    assert expected_data == response.get_json().get('data')


def test_finish_task(client):
    setup_task(client)
    update_data = {
        "complete": True,
    }
    expected_data = {
        "id": 1,
        "name": "Do this",
        "todo_id": 1,
        "description": "Do this properly",
        "complete": True
    }
    response = client.put('/tasks/1', headers=headers, data=json.dumps(update_data))
    assert "200" in response.status
    assert expected_data == response.get_json().get('data')
