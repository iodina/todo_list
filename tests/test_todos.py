from operator import itemgetter
import pytest
import json


headers = {
    "Content-Type": "application/json"
}


def setup_todos(client, todos=None):
    test_todos = todos or [
        {
            "name": "My nice TODO"
        },
        {
            "name": "My boring TODO"
        }]
    response = client.post('/todos', data=json.dumps(test_todos), headers=headers)
    assert '201' in response.status


def test_no_todos(client):
    response = client.get('/todos', headers=headers)
    assert '200' in response.status
    assert [] == response.get_json().get('data')


@pytest.mark.parametrize("test_todo_data", [
    [{
        "name": "My nice TODO"
    },{
        "name": "My boring TODO"
    }],
    [{
        "name": "My nice TODO"
    }],
    []
])
def test_bulk_create_todos_success(client, test_todo_data):
    response = client.post('/todos', data=json.dumps(test_todo_data), headers=headers)
    assert '201' in response.status
    all(pair[0]["name"] == pair[1]["name"] for pair in zip(sorted(test_todo_data,
                                                                  key=itemgetter("name")),
                                                           sorted(response.get_json().get('data'),
                                                                  key=itemgetter("name"))))


@pytest.mark.parametrize("test_todo_data", [
    {
        "name": "My nice TODO"
    },
    {},
    [{
        "fake_name": "My nice TODO"
    }],
])
def test_bulk_create_todos_failure(client, test_todo_data):
    response = client.post('/todos', data=json.dumps(test_todo_data), headers=headers)
    assert '422' in response.status


def test_get_todos_success(client):
    setup_todos(client)
    expected_todos = [
        {
            "id": 1,
            "name": "My nice TODO"
        },
        {
            "id": 2,
            "name": "My boring TODO"
        }
    ]

    # Get all todos
    response = client.get('/todos', headers=headers)
    assert '200' in response.status
    assert sorted(expected_todos, key=itemgetter("id")) == sorted(response.get_json().get('data'), key=itemgetter("id"))


def test_get_todo_by_id(client):
    setup_todos(client)
    expected_todos = {
        "id": 1,
        "name": "My nice TODO"
    }
    response = client.get('/todos/1', headers=headers)
    assert '200' in response.status
    assert expected_todos == response.get_json().get('data')
