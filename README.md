# README #

Simple TODO List application

## How do I get set up? ##

### Set Up Dependencies: ###

---
    $ python3.7 -m venv env
    $ source env/bin/activate
    (env)$ pip install -r requirements.txt
---

### Database configuration: ###

#### 1. Create db
---
    $ sqlite3 todo_app.db
---
#### 2. Add environment variable DATABASE_URL pointing to db uri
---
    DATABASE_URL='sqlite:///<path_to_db_file>'
---
Note: 
    If this step is skipped, code will assume there is db named todo_app.db in root dir

#### 3. Apply migrations
---
    $ cd <project_directory>  
    $ flask db upgrade
---

### How to run tests ###
---
    $ cd <project_directory> 
    $ pytest -v tests  --junitxml=test-reports/report.xml
---

### How to run application ###
---
    $ cd <project_directory> 
    $ flask run
---

## Usage examples ##

### 1) Create multiple TODO lists: ###

    POST 
        /todo

    Payload:
    [{
        "name": "todo_name"
    },
    {
        "name": "another_todo_name"
    }]
    
    Response:
    {"data": [
        {
            "id": 1,
            "name": "todo_name"
        },
        {
            "id": 2,
            "name": "another_todo_name"
        }
    ]}

### 2) Add a task to the TODO list: ###
    POST 
        /task
    
    Payload:
    {
        "name": "Do this",
        "todo_id": 1,
        "description": "Do this properly",
        "complete": false
    }
    
    Response:
    {"data": {
        "id": 3,
        "todo_id": 1,
        "name": "Do this",
        "description": "Do this properly",
        "complete": false
    }}


### 3) Get all tasks from one TODO list: ###

    GET 
        /task?todo_id=<todo id>
    
    Response:
    {"data": [
        {
            "id": 3,
            "todo_id": 1,
            "name": "Do this",
            "description": "Do this properly",
            "complete": false
        }
    ]}

### 4) Delete task: ###

    DELETE
        /task/<id>
    
    Response:
    {"data": true}
    
### 5) Edit task: ###

    PUT
        /task/<id>
    
    Payload:
    {
        "description": "Don't Do this properly"
    }
    
    Response:
    {"data": {
        "id": 2,
        "todo_id": 1,
        "name": "Do this",
        "description": "Don't Do this properly",
        "complete": false
    }}

### 6) Finish task: ###

    PUT
        /task/<id>

    Payload:
    {
        "complete": true
    }
    
    Response:
    {"data": {
        "id": 2,
        "todo_id": 1,
        "name": "Do this",
        "description": "Don't Do this properly",
        "complete": true
    }}