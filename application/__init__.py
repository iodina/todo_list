from flask import Flask
from flask_migrate import Migrate
from config import Config
from application.resourses.healthcheck import HealthcheckResource
from application.extensions import db

from application.resourses.task import task_api, task_api_bp
from application.resourses.todo import todo_api, todo_api_bp
from application.resourses.healthcheck import healthcheck_api, healthcheck_bp

app = Flask(__name__)
app.config.from_object(Config)

db.init_app(app)
migrate = Migrate(app, db)

app.register_blueprint(task_api_bp)
app.register_blueprint(todo_api_bp)
app.register_blueprint(healthcheck_bp)
