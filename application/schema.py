from marshmallow import Schema, fields


class TodoPostSchema(Schema):
    """ /todos - POST

    Parameters:
     - name (str)
    """
    name = fields.Str(required=True)


class TaskPostSchema(Schema):
    """ /tasks - POST

    Parameters:
     - name (str)
     - description (str)
     - todo_id (int)
     - complete (bool)

    """
    name = fields.Str(required=True)
    description = fields.Str(required=True)
    todo_id = fields.Int(required=True)
    complete = fields.Bool(default=False)


class TaskPutSchema(Schema):
    """ /tasks - PUT

    Parameters:
     - name (str)
     - description (str)
     - todo_id (int)
     - complete (bool)

    """
    name = fields.Str()
    description = fields.Str()
    todo_id = fields.Int()
    complete = fields.Bool()
