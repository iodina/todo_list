from flask_restful import Resource, Api
from flask import jsonify, Blueprint


healthcheck_bp = Blueprint('healthcheck_bp', __name__)
healthcheck_api = Api(healthcheck_bp)


class HealthcheckResource(Resource):

    def get(self):
        return jsonify({'hello': 'world'})


healthcheck_api.add_resource(HealthcheckResource, '/')
