from http import HTTPStatus
from flask_restful import Resource, Api
from flask import request, Blueprint, abort

from application.domain.exceptions import TodoNotFoundError
from application.schema import TaskPostSchema, TaskPutSchema
from application.domain.task import TaskEntity


task_api_bp = Blueprint('task_api_bp', __name__, )
task_api = Api(task_api_bp)


class TaskResource(Resource):

    def get(self, _id=None):
        filter_args = request.args
        data = TaskEntity().get(_id=_id, filters=filter_args)
        return {'data': data}

    def post(self):
        data = request.get_json()
        errors = TaskPostSchema().validate(data)
        if errors:
            abort(HTTPStatus.UNPROCESSABLE_ENTITY, str(errors))
        try:
            data = TaskEntity().add_task(data)
        except TodoNotFoundError as err:
            abort(HTTPStatus.NOT_FOUND, str(err))
        return {'data': data}, HTTPStatus.CREATED

    def put(self, _id=None):
        data = request.get_json()
        if not _id:
            abort(HTTPStatus.UNPROCESSABLE_ENTITY, "Object id must be provided")
        errors = TaskPutSchema().validate(data)
        if errors:
            abort(HTTPStatus.UNPROCESSABLE_ENTITY, str(errors))
        data = TaskEntity().update(_id=_id, data=data)
        return {'data': data}

    def delete(self, _id):
        TaskEntity().delete(_id=_id)
        return {'data': True}


task_api.add_resource(TaskResource, '/tasks', '/tasks/', '/tasks/<_id>')
