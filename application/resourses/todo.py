from http import HTTPStatus
from flask_restful import Resource, Api
from flask import request, Blueprint, abort

from application.schema import TodoPostSchema
from application.domain.todo import TodoEntity


todo_api_bp = Blueprint('todo_api_bp', __name__)
todo_api = Api(todo_api_bp)


class TodoResource(Resource):

    def get(self, _id=None):
        data = TodoEntity().get(_id=_id)
        return {'data': data}

    def post(self):
        data = request.get_json()
        errors = TodoPostSchema(many=True).validate(data)
        if errors:
            abort(HTTPStatus.UNPROCESSABLE_ENTITY, str(errors))
        data = TodoEntity().bulk_insert(data=data)
        return {'data': data}, HTTPStatus.CREATED


todo_api.add_resource(TodoResource, '/todos', '/todos/', '/todos/<_id>')
