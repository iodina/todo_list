from application.domain import BaseEntity
from application.models import Todo


class TodoEntity(BaseEntity):

    model = Todo
