from application.domain import BaseEntity
from application.domain.exceptions import TodoNotFoundError
from application.models import Task
from application.domain.todo import TodoEntity


class TaskEntity(BaseEntity):

    model = Task

    def add_task(self, data):
        todo = TodoEntity().get_by_id(data["todo_id"])
        if not todo:
            raise TodoNotFoundError("There is no such Todo List")
        return self.insert(data)





