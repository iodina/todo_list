from abc import abstractmethod, ABC
from application.extensions import db


class BaseEntity(ABC):

    @property
    @abstractmethod
    def model(self):
        pass

    def get_by_id(self, _id):
        return self.model.query.get(_id)

    def get_all(self, **filters):
        if filters:
            return self.model.query.filter_by(**filters).all()
        return self.model.query.all()

    def get(self, _id=None, filters=None):
        if _id:
            record = self.get_by_id(_id)
            return record.serialize() if record else {}

        filters = {key: value[0] for key, value in dict(filters).items()} if filters else {}
        return list(map(lambda record: record.serialize(), self.get_all(**filters)))

    def insert(self, data):
        data = self.model(**data)
        db.session.add(data)
        db.session.commit()
        return data.serialize()

    def bulk_insert(self, data):
        data_list = [self.model(**d) for d in data]
        db.session.add_all(data_list)
        db.session.commit()
        return list(map(lambda record: record.serialize(), data_list))

    def update(self, _id, data=None):
        if data:
            self.model.query.filter_by(id=_id).update(data)
            db.session.commit()
        record = self.model.query.get(_id)
        return record.serialize() if record else {}

    def delete(self, _id):
        db.session.delete(self.model.query.get(_id))
        db.session.commit()
