from application.extensions import db


class Todo(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)

    def serialize(self):
        return {"id": self.id,
                "name": self.name}


class Task(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    todo_id = db.Column(db.Integer, db.ForeignKey('todo.id'), nullable=False)
    name = db.Column(db.String(64), nullable=False)
    description = db.Column(db.String(250))
    complete = db.Column(db.Boolean, default=False)

    def serialize(self):
        return {"id": self.id,
                "todo_id": self.todo_id,
                "name": self.name,
                "description": self.description,
                "complete": self.complete
                }

